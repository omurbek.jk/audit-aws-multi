coreo_aws_rule "administrative-policy-exposed-by-connected-ssh-credential" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-connected-threats-ssh-credentials"
  display_name "Publicly routable instance shares ssh-key with administrative instances"
  description "A publicly routable and addressable ec2 instance has the same ssh key as an instance with an administrative policy."
  category "Security"
  suggested_action "Generate distinct ssh keys per subnet or ec2 instance role."
  level "High"
  objectives ["describe_internet_gateways"]
  audit_objects ["object.internet_gateways.internet_gateway_id"]
  operators ["=~"]
  raise_when [//]
  id_map ["object.internet_gateways.internet_gateway_id"]
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) @cascade {
        <%= default_predicates %>
        relates_to @filter(<%= filter['route'] %>) {
          <%= default_predicates %>
          relates_to @filter(<%= filter['route_table'] %>) {
            <%= default_predicates %>
            relates_to @filter(<%= filter['route_table_association'] %>) {
              <%= default_predicates %>
              relates_to @filter(<%= filter['subnet'] %>) {
                <%= default_predicates %>
                pub_instances as relates_to @filter(<%= filter['instance'] %> AND has(public_ip_address)) {
                  <%= default_predicates %>
                  exposed_keys as relates_to @filter(<%= filter['key_pair'] %>){
                    <%= default_predicates %>
                    relates_to @filter(<%= filter['instance'] %> AND NOT uid(pub_instances)) {
                      <%= default_predicates %>
                      relates_to @filter(<%= filter['iam_instance_profile'] %>){
                        <%= default_predicates %>
                        relates_to @filter(<%= filter['role'] %>){
                          <%= default_predicates %>
                          exposed_policies as relates_to @filter(<%= filter['policy'] %>){
                            <%= default_predicates %>
                            exposed_policy_arns as policy_arn
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        
      }
    }
    query(func: uid(gateways)) @cascade {
      <%= default_predicates %>
      relates_to @filter(<%= filter['route'] %>) {
        <%= default_predicates %>
        relates_to @filter(<%= filter['route_table'] %>) {
          <%= default_predicates %>
          relates_to @filter(<%= filter['route_table_association'] %>) {
            <%= default_predicates %>
            relates_to @filter(<%= filter['subnet'] %>) {
              <%= default_predicates %>
              relates_to @filter(<%= filter['instance'] %>) {
                <%= default_predicates %>
                relates_to @filter(uid(exposed_keys)){
                  <%= default_predicates %>
                  relates_to @filter(<%= filter['instance'] %>){
                    <%= default_predicates %>
                    relates_to @filter(<%= filter['iam_instance_profile'] %>){
                      <%= default_predicates %>
                      relates_to @filter(<%= filter['role'] %>){
                        <%= default_predicates %>
                        relates_to @filter(uid(exposed_policies) AND eq(val(exposed_policy_arns), "arn:aws:iam::aws:policy/AdministratorAccess")){
                          <%= default_predicates %>
                          policy_name policy_arn                      
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({
    'internet_gateway' => ['relates_to'], 
    'route' => [], 
    'route_table' => [], 
    'route_table_association' => [], 
    'iam_instance_profile' => [], 
    'role' => [],
    'policy' => [],
    'key_pair' => []
  })
end

coreo_aws_rule "instance-has-public-access-and-full-s3-privileges" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-connected-threats-public-instance-with-full-s3-privileges"
  display_name "Publicly routable instance has full S3 iam privileges"
  description "A publicly accessible instance can make any changes to any S3 bucket or bucket ACLs."
  category "Security"
  suggested_action "Remove this privilege from any publicly accessable instance."
  level "High"
  objectives ["describe_internet_gateways"]
  audit_objects ["object.internet_gateways.internet_gateway_id"]
  operators ["=~"]
  raise_when [//]
  id_map ["object.internet_gateways.internet_gateway_id"]
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func:  <%= filter['route'] %>) { }
    tables as var(func:  <%= filter['route_table'] %>) { }
    associations as var(func:  <%= filter['route_table_association'] %>) { }
    subnets as var(func:  <%= filter['subnet'] %>) { }
    pub_instances as var(func:  <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    profiles as var(func:  <%= filter['iam_instance_profile'] %>) { }
    roles as var(func:  <%= filter['role'] %>) { }
    policies as var(func:  <%= filter['policy'] %>) @cascade {
      policy_arns as policy_arn
    }
    s3_full_access_policy as q(func: uid(policies)) @filter(eq(val(policy_arns), "arn:aws:iam::aws:policy/AmazonS3FullAccess")) { }
    pub_instances_with_s3_policy as var(func: uid(pub_instances)) @cascade {
      filtered_profiles as relates_to @filter(uid(profiles)) {
        filtered_roles as relates_to @filter(uid(roles)) {
          filtered_policies as relates_to @filter(uid(s3_full_access_policy))
        }
      }
    }
    pub_instances_with_gateways as var(func: uid(pub_instances)) @cascade {
      filtered_subnets as relates_to @filter(uid(subnets)) {
        filtered_associations as relates_to @filter(uid(associations)) {
          filtered_tables as relates_to @filter(uid(tables)) {
            filtered_routes as relates_to @filter(uid(routes)) {
              filtered_gateways as relates_to @filter(uid(gateways))
            }
          }
        }
      }
    }
    query(func: uid(pub_instances_with_s3_policy)) @filter(uid(pub_instances_with_gateways)) {
      <%= default_predicates %>
      public_ip_address
      public_dns_name
      relates_to @filter(uid(filtered_profiles) OR uid(filtered_subnets)) {
        <%= default_predicates %>
        instance_profile_name
        cidr_block tags
        relates_to @filter(uid(filtered_roles) OR uid(filtered_associations)) {
          <%= default_predicates %>
          role_name
          description
          assume_role_policy_document
          implicit_route_association: main
          relates_to @filter(uid(filtered_policies) OR uid(filtered_tables)) {
            <%= default_predicates %>
            policy_arn
            relates_to @filter(uid(filtered_routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(filtered_gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({
    'internet_gateway' => [],
    'route' => [],
    'route_table' => [],
    'route_table_association' => [],
    'subnet' => [],
    'instance' => ['public_ip_address'],
    'iam_instance_profile' => [],
    'role' => [],
    'policy' => ['policy_arn']
  })
end

coreo_aws_rule "aws-guardduty-port-probe-unprotected-port" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-port-probe-unprotected-port.html"
  display_name "AWS GuardDuty PortProbeUnprotectedPort"
  description "GuardDuty detected port probe(s) on an instance's network interface."
  category "Recon"
  suggested_action "Block access to any unused ports using a combination of security groups network ACS's and host-based firewall rules"
  level "Low"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:EC2/PortProbeUnprotectedPort")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-tor-ip-caller-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-tor-ip-caller-recon-iamuser.html"
  display_name "AWS GuardDuty TorIPCaller - Recon IAMUser"
  description "An API was invoked from a Tor exit node IP address."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/TorIPCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-malicious-ip-caller.custom-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-malicious-ip-caller.custom-recon-iamuser.html"
  display_name "AWS GuardDuty MaliciousIPCaller.Custom - Reconnaissance IAMUser"
  description "An IAM list/describe API was invoked from an IP address in your custom threat list."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/MaliciousIPCaller.Custom")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-malicious-ip-caller-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-malicious-ip-caller-recon-iamuser.html"
  display_name "AWS GuardDuty MaliciousIPCaller - Reconnaissance IAMUser"
  description "An IAM list/describe API was invoked from a known malicious IP address."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/MaliciousIPCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-portscan" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-portscan.html"
  display_name "AWS GuardDuty Portscan"
  description "EC2 instance is performing outbound port scans to a remote host."
  category "Recon"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:EC2/Portscan")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-network-permissions-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-network-permissions-recon-iamuser.html"
  display_name "AWS GuardDuty NetworkPermissions - Persistence IAMUser"
  description "An IAM user invoked an API commonly used to discover the network access permissions of existing security groups, ACLs, and routes in your AWS account."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/NetworkPermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-resource-permissions-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-resource-permissions-recon-iamuser.html"
  display_name "AWS GuardDuty ResourcePermissions - Recon IAMUser"
  description "An IAM user invoked an API commonly used to discover the permissions associated with various resources in your AWS account."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/ResourcePermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-user-permissions-recon-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-user-permissions-recon-iamuser.html"
  display_name "AWS GuardDuty UserPermissions - Recon IAMUser"
  description "An IAM user invoked an API commonly used to discover the users, groups, policies and permissions in your AWS account."
  category "Recon"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Recon:IAMUser/UserPermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-xorddos" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-xorddos.html"
  display_name "AWS GuardDuty XORDDOS"
  description "An EC2 instance is attempting to communicate with an IP address that is associated with XorDDos malware."
  category "Backdoor"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Backdoor:EC2/XORDDOS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-spambot" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-spambot.html"
  display_name "AWS GuardDuty Spambot"
  description "EC2 instance is exhibiting unusual behavior by communicating with a remote host on port 25."
  category "Backdoor"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Backdoor:EC2/Spambot")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-c-and-c-activity.b-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-c-and-c-activity.b-not-dns.html"
  display_name "AWS GuardDuty C&CActivity.B!DNS"
  description "EC2 instance is querying a domain name that is associated with a known command and control server."
  category "Backdoor"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Backdoor:EC2/C&CActivity.B!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-network-port-unusual" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-network-port-unusual.html"
  display_name "AWS GuardDuty NetworkPortUnusual"
  description "EC2 instance is communicating with a remote host on an unusual server port."
  category "Behavior"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Behavior:EC2/NetworkPortUnusual")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-traffic-volume-unusual" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-traffic-volume-unusual.html"
  display_name "AWS GuardDuty TrafficVolumeUnusual"
  description "EC2 instance is generating unusually large amounts of network traffic to a remote host."
  category "Backdoor"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Behavior:EC2/TrafficVolumeUnusual")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-bitcoin-tool.b-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-bitcoin-tool.b-not-dns.html"
  display_name "AWS GuardDuty BitcoinTool.B!DNS"
  description "EC2 instance is querying a domain name that is associated with cryptocurrency-related activity."
  category "CryptoCurrency"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "CryptoCurrency:EC2/BitcoinTool.B!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-bitcoin-tool.b" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-bitcoin-tool.b.html"
  display_name "AWS GuardDuty BitcoinTool.B"
  description "EC2 instance is querying an IP address that is associated with cryptocurrency-related activity."
  category "CryptoCurrency"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "CryptoCurrency:EC2/BitcoinTool.B")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-kali-linux" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-kali-linux.html"
  display_name "AWS GuardDuty KaliLinux"
  description "An API was invoked from a Kali Linux EC2 instance."
  category "PenTest"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "PenTest:IAMUser/KaliLinux")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-network-permissions-persistence-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-network-permissions-persistence-iamuser.html"
  display_name "AWS GuardDuty NetworkPermissions - Persistence IAMUser"
  description "An IAM user invoked an API commonly used to change the network access permissions for security groups, routes, and ACLs in your AWS account."
  category "Persistence"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Persistence:IAMUser/NetworkPermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-resource-permissions-persistence-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-resource-permissions-persistence-iamuser.html"
  display_name "AWS GuardDuty ResourcePermissions - Persistence IAMUser"
  description "An IAM user invoked an API commonly used to change the security access policies of various resources in your AWS account."
  category "Persistence"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Persistence:IAMUser/ResourcePermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-user-permissions-persistence-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-user-permissions-persistence-iamuser.html"
  display_name "AWS GuardDuty UserPermissions - Persistence IAMUser"
  description "An IAM user invoked an API commonly used to add, modify, or delete IAM users, groups or policies in your AWS account."
  category "Persistence"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Persistence:IAMUser/UserPermissions")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-compute-resources" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-compute-resources.html"
  display_name "AWS GuardDuty ComputeResources"
  description "An IAM user invoked an API commonly used to launch compute resources like EC2 Instances."
  category "ResourceConsumption"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "ResourceConsumption:IAMUser/ComputeResources")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-password-policy-change" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-password-policy-change.html"
  display_name "AWS GuardDuty PasswordPolicyChange"
  description "Account password policy was weakened."
  category "Stealth"
  suggested_action "Verify that changed password policy is desired and which credential was used to make the configuration change."
  level "Low"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Stealth:IAMUser/PasswordPolicyChange")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-cloud-trail-logging-disabled" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-cloud-trail-logging-disabled.html"
  display_name "AWS GuardDuty CloudTrailLoggingDisabled"
  description "AWS CloudTrail trail was disabled."
  category "Stealth"
  suggested_action "Verify that disabled CloudTrail is desired and which credential was used to make the configuration change."
  level "Low"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Stealth:IAMUser/CloudTrailLoggingDisabled")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-logging-configuration-modified" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-logging-configuration-modified.html"
  display_name "AWS GuardDuty LoggingConfigurationModified"
  description "An IAM user invoked an API commonly used to stop CloudTrail logging, delete existing logs, and otherwise eliminate traces of activity in your AWS account."
  category "Stealth"
  suggested_action "Verify that disabled CloudTrail is desired and which credential was used to make the configuration change."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Stealth:IAMUser/LoggingConfigurationModified")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-blackhole-traffic" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-blackhole-traffic.html"
  display_name "AWS GuardDuty BlackholeTraffic"
  description "EC2 instance is attempting to communicate with an IP address of a remote host that is a known black hole."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/BlackholeTraffic")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-drop-point" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-drop-point.html"
  display_name "AWS GuardDuty DropPoint"
  description "An EC2 instance is attempting to communicate with an IP address of a remote host that is known to hold credentials and other stolen data captured by malware."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DropPoint")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-blackhole-traffic-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-blackhole-traffic-not-dns.html"
  display_name "AWS GuardDuty BlackholeTraffic!DNS"
  description "EC2 instance is querying a domain name that is being redirected to a black hole IP address."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/BlackholeTraffic!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-drive-by-source-traffic-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-drive-by-source-traffic-not-dns.html"
  display_name "AWS GuardDuty DriveBySourceTraffic!DNS"
  description "EC2 instance is querying a domain name of a remote host that is a known source of Drive-By download attacks."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DriveBySourceTraffic!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-drop-point-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-drop-point-not-dns.html"
  display_name "AWS GuardDuty DropPoint!DNS"
  description "An EC2 instance is querying a domain name of a remote host that is known to hold credentials and other stolen data captured by malware."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DropPoint!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-dga-domain-request.b" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-dga-domain-request.b.html"
  display_name "AWS GuardDuty DGADomainRequest.B"
  description "EC2 instance is querying algorithmically generated domains. Such domains are commonly used by malware and could be an indication of a compromised EC2 instance."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DGADomainRequest.B")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-dga-domain-request.c-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-dga-domain-request.c-not-dns.html"
  display_name "AWS GuardDuty DGADomainRequest.C!DNS"
  description "EC2 instance is querying algorithmically generated domains. Such domains are commonly used by malware and could be an indication of a compromised EC2 instance."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DGADomainRequest.C!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-dns-data-exfiltration" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-dns-data-exfiltration.html"
  display_name "AWS GuardDuty DNSDataExfiltration"
  description "EC2 instance is exfiltrating data through DNS queries."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/DNSDataExfiltration")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-phishing-domain-request-not-dns" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-phishing-domain-request-not-dns.html"
  display_name "AWS GuardDuty PhishingDomainRequest!DNS"
  description "EC2 instance is querying domains involved in phishing attacks. Your EC2 instance might be compromised."
  category "Trojan"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "Trojan:EC2/PhishingDomainRequest!DNS")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-tor-ip-caller-unauthorized-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-tor-ip-caller-unauthorized-iamuser.html"
  display_name "AWS GuardDuty TorIPCaller - Unauthorized IAMUser"
  description "An API was invoked from a Tor exit node IP address."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/TorIPCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-malicious-ip-caller.custom-unauthorized-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-malicious-ip-caller.custom-unauthorized-iamuser.html"
  display_name "AWS GuardDuty MaliciousIPCaller.Custom - Unauthorized IAMUser"
  description "An IAM modify/write API was invoked from an IP address in your custom threat list."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/MaliciousIPCaller.Custom")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-console-login-success.b" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-console-login-success.b.html"
  display_name "AWS GuardDuty ConsoleLoginSuccess.B"
  description "Multiple worldwide successful console logins were observed."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/ConsoleLoginSuccess.B")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-malicious-ip-caller-unauthorized-iamuser" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-malicious-ip-caller-unauthorized-iamuser.html"
  display_name "AWS GuardDuty MaliciousIPCaller - Unauthorized IAMUser"
  description "An IAM modify/write API was invoked from a known malicious IP address."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/MaliciousIPCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-unusual-asn-caller" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-unusual-asn-caller.html"
  display_name "AWS GuardDuty UnusualASNCaller"
  description "An API was invoked from an IP address of an unusual network."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/UnusualASNCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-tor-ip-caller-ec2-iamuser" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-tor-ip-caller-ec2-iamuser.html"
  display_name "AWS GuardDuty TorIPCaller - EC2 IAMUser"
  description "EC2 instance is receiving inbound connections from a Tor exit node."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/TorIPCaller")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-malicious-ip-caller.custom-unauthorized-ec2" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-malicious-ip-caller.custom-unauthorized-ec2.html"
  display_name "AWS GuardDuty MaliciousIPCaller.Custom - Unauthorized EC2"
  description "EC2 instance is communicating outbound with a IP address on a custom threat list."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/MaliciousIPCaller.Custom")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-ssh-brute-force" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-ssh-brute-force.html"
  display_name "AWS GuardDuty SSHBruteForce"
  description "EC2 instance has been involved in SSH brute force attacks."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Low"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/SSHBruteForce")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-rdp-brute-force" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-rdp-brute-force.html"
  display_name "AWS GuardDuty RDPBruteForce"
  description "EC2 instance has been involved in RDP brute force attacks."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "Low"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/RDPBruteForce")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-instance-credential-exfiltration" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-instance-credential-exfiltration.html"
  display_name "AWS GuardDuty InstanceCredentialExfiltration"
  description "Credentials that were created exclusively for an EC2 instance through an instance launch role are being used from an external IP address."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/InstanceCredentialExfiltration")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-console-login" do
  action :define
  service :iam 
  link "https://kb.securestate.vmware.com/aws-gd-console-login.html"
  display_name "AWS GuardDuty ConsoleLogin"
  description "An unusual console login by an IAM user in your AWS account was observed."
  category "UnauthorizedAccess"
  suggested_action "Verify that IAM credentials used for this API access are valid and needed. Change credentials if there is any doubt."
  level "Medium"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:IAMUser/ConsoleLogin")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-tor-client" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-tor-client.html"
  display_name "AWS GuardDuty TorClient"
  description "EC2 instance is making connections to a Tor Guard or an Authority node."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/TorClient")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "aws-guardduty-tor-relay" do
  action :define
  service :ec2 
  link "https://kb.securestate.vmware.com/aws-gd-tor-relay.html"
  display_name "AWS GuardDuty TorRelay"
  description "EC2 instance is making connections to a Tor network as a Tor relay."
  category "UnauthorizedAccess"
  suggested_action "Verify health of associated EC2 instance and remediate if necessary."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [//]
  id_map [""]
  meta_rule_query <<~QUERY
  {
    findings as var(func: <%= filter['guard_duty_finding'] %>) {
      f_type as finding_type 
    }
    instances as var(func: has(instance)) {}
    accounts as var(func: has(account)) {}
    gd_services           as var(func: <%= filter['guard_duty_service'] %>) {}
    gd_actions            as var(func: <%= filter['guard_duty_action'] %>) {}
    gd_port_probe_actions as var(func: <%= filter['guard_duty_port_probe_action'] %>) {}
    gd_port_probe_details as var(func: <%= filter['guard_duty_port_probe_detail'] %>) {}
    gd_remote_ip_details  as var(func: <%= filter['guard_duty_remote_ip_detail'] %>) {}
    gd_local_port_details as var(func: <%= filter['guard_duty_local_port_detail'] %>) {}
    gd_countries          as var(func: <%= filter['guard_duty_country'] %>) {}
    gd_remote_ip_org      as var(func: <%= filter['guard_duty_remote_ip_org'] %>) {}

    base_query as query(func: uid(findings)) @filter(eq(val(f_type), "UnauthorizedAccess:EC2/TorRelay")) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
    }
    visualize(func: uid(base_query)) {
      <%= default_predicates %>
      finding_type
      created_at
      updated_at
      title
      description
      relates_to @filter(uid(instances, accounts, gd_services)) {
        <%= default_predicates %>
        instance_id
        public_dns_name
        public_ip_address
        private_dns_name
        private_dns_address
        AWS_account:account_id
        event_first_seen
        event_last_seen
        event_count:count
        relates_to @filter(uid(gd_actions)) {
          <%= default_predicates %>
          action_type
          relates_to @filter(uid(gd_port_probe_actions)) {
            <%= default_predicates %>
            relates_to @filter(uid(gd_port_probe_details)) {
              <%= default_predicates %>
              relates_to @filter(uid(gd_remote_ip_details, gd_local_port_details)) {
                <%= default_predicates %>
                port
                port_name
                ip_address_v4
                geo_location_lat
                geo_location_lon
                relates_to @filter(uid(gd_countries, gd_remote_ip_org)) {
                  <%= default_predicates %>
                  country_name
                  isp
                  org
                  asn
                  asn_org
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                            'guard_duty_finding' => [],
                            'guard_duty_service' => ['event_last_seen', 'count'],
                            'guard_duty_action' => [],
                            'guard_duty_port_probe_action' => [],
                            'guard_duty_port_probe_detail' => [],
                            'guard_duty_remote_ip_detail' => [],
                            'guard_duty_local_port_detail' => [],
                            'guard_duty_country' => [],
                            'guard_duty_remote_ip_org' => []
                          })
end

coreo_aws_rule "public-instance" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance"
  description "Instance is publicly accessiible."
  category "Security"
  suggested_action "Consider making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    query(func: uid(pub_ip_instances)) @cascade {
      <%= default_predicates %>
      relates_to @filter(uid(subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-admin-role-instance" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance with Admin Role"
  description "Instance is publicly accessiible and has administrator role."
  category "Security"
  suggested_action "Consider making instance private and/or remove attached role."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    instance_profiles as var(func: <%= filter['iam_instance_profile'] %>) { }
    roles as var(func: <%= filter['role'] %>) { }
    policies as var(func: <%= filter['policy'] %>) @cascade {
      pname as policy_name
    }
    admin_roles as var(func: uid(roles)) @cascade {
      relates_to @filter(uid(policies) AND eq(val(pname), "AdministratorAccess"))
    }
    pub_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    pub_admin_instances as var(func: uid(pub_instances)) @cascade {
      relates_to @filter(uid(instance_profiles)) {
        relates_to @filter(uid(admin_roles))
      }
    }
    query(func: uid(pub_admin_instances)) {
      <%= default_predicates %>
      role_name
      arn
      public_ip_address
      relates_to @filter(uid(policies, instance_profiles, subnets)) {
        <%= default_predicates %>
        policy_name
        arn
        public_ip_address
        instance_profile_name
        relates_to @filter(uid(admin_roles, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-1521" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 1521"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 1521)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-3306" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 3306"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 3306)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-5432" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 5432"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 5432)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-27017" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 27017"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 27017)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-1433" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 1433"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 1433)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-3389" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 3389"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 3389)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-22" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 22"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 22)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-world-open-TCP-5439" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 5439"
  description "Public instance is open on important TCP port to the world."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  # TODO resolve for IPv6
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) @cascade {
      start as range_start
      end as range_end
      open as math(end - start == <%= 2**32 - 1 %>)
    }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 5439)) { }
    open_ranges as var(func: uid(ranges)) @filter(eq(val(open), true)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(open_ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(open_ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-open-TCP-23" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 23"
  description "Public instance is open on important TCP port."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) { }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 23)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-open-TCP-21" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 21"
  description "Public instance is open on important TCP port."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) { }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 21)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-open-TCP-20" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 20"
  description "Public instance is open on important TCP port."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
   objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) { }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 20)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end

coreo_aws_rule "public-instance-open-TCP-8080" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-ec2-tcpportopen.html"
  display_name "Public Instance TCP port is open - 8080"
  description "Public instance is open on important TCP port."
  category "Security"
  suggested_action "Only open those ports that must be open for your service to operate. Consider deleting or modifying the affected security group and/or making instance private."
  level "High"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_rule_query <<~QUERY
  {
    sg as var(func: <%= filter['security_group'] %>) { }
    ip as var(func: <%= filter['ip_permission'] %>) @cascade {
      protocol as ip_protocol
      port as from_port
    }
    ranges as var(func: <%= filter['ip_range'] %>) { }
    tcp_permissions as var(func: uid(ip)) @filter(eq(val(protocol), "tcp") AND eq(val(port), 8080)) { }
    open_sg as var(func: uid(sg)) @cascade {
      relates_to @filter(uid(tcp_permissions)) {
        relates_to @filter(uid(ranges)) { }
      }
    }
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func: <%= filter['route'] %>) { }
    rtables as var(func: <%= filter['route_table'] %>) { }
    rt_assoc as var(func: <%= filter['route_table_association'] %>) { }
    subnets as var(func: <%= filter['subnet'] %>) { }
    pub_ip_instances as var(func: <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    public_instances as var(func: uid(pub_ip_instances)) @cascade {
      relates_to @filter(uid(subnets)) {
        relates_to @filter(uid(rt_assoc)) {
          relates_to @filter(uid(rtables)) {
            relates_to @filter(uid(routes)) {
              relates_to @filter(uid(gateways)) { }
            }
          }
        }
      }
    }
    open_pub_instances as var(func: uid(public_instances)) @cascade {
      relates_to @filter(uid(open_sg))
    }
    query(func: uid(open_pub_instances)) {
      <%= default_predicates %>
      relates_to @filter(uid(open_sg, subnets)) {
        <%= default_predicates %>
        relates_to @filter(uid(tcp_permissions, rt_assoc)) {
          <%= default_predicates %>
          relates_to @filter(uid(ranges, rtables)) {
            <%= default_predicates %>
            relates_to @filter(uid(routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({})
end
