#!/usr/bin/env ruby

require 'csv'
require 'erb'

this_dir = File.expand_path(File.dirname(__FILE__))
tempate_dir = File.join(this_dir, 'templates')
fixture_dir = File.join(this_dir, 'fixtures')
output_dir = File.join(this_dir, 'output')

template = File.read(File.join(tempate_dir, 'gd-rule.rb.erb'))
@rows = []
CSV.foreach(File.join(fixture_dir, 'gd-rules.csv'), headers: true, header_converters: :symbol) do |row|
  @rows << row
end
renderer = ERB.new(template, nil, '-')
File.open(File.join(output_dir, 'gd-rule.rb.out'), 'w') do |f|
  f.write(renderer.result(binding))
end
